import React, { PropTypes } from 'react';

const Index = () => {
    return (
        <div>Hi there</div>
    );
};

Index.displayName = 'Index';

Index.propTypes = {};

export default Index;
