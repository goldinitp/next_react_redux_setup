"use strict";

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _express = _interopRequireDefault(require("express"));

var _idx = _interopRequireDefault(require("idx"));

var _next2 = _interopRequireDefault(require("next"));

var _cookieParser = _interopRequireDefault(require("cookie-parser"));

var _compression = _interopRequireDefault(require("compression"));

var _expressUseragent = _interopRequireDefault(require("express-useragent"));

var _expressSession = _interopRequireDefault(require("express-session"));

var _connectRedis = _interopRequireDefault(require("connect-redis"));

var _querystring = _interopRequireDefault(require("querystring"));

var _raven = _interopRequireDefault(require("raven"));

var _morgan = _interopRequireDefault(require("morgan"));

var _axios = _interopRequireDefault(require("axios"));

var _bodyParser = _interopRequireDefault(require("body-parser"));

var _Security = _interopRequireDefault(require("./middlewares/Security"));

var _RateLimiter = _interopRequireDefault(require("./middlewares/RateLimiter"));

var _serverConfig = _interopRequireDefault(require("./server-config"));

var _redisConfig = _interopRequireDefault(require("./redis-config"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } } function _next(value) { step("next", value); } function _throw(err) { step("throw", err); } _next(); }); }; }

// import authenticParams from '../constants/urlParams';
require('dotenv').config();

var _process$env = process.env,
    PORT = _process$env.PORT,
    NODE_ENV = _process$env.NODE_ENV,
    API_BASE_URL = _process$env.API_BASE_URL,
    ABTASTY_BASE_URL = _process$env.ABTASTY_BASE_URL,
    ABTASTY_API_KEY = _process$env.ABTASTY_API_KEY;
var dev = NODE_ENV !== 'production';
var port = PORT ? parseInt(PORT, 10) : 3000;
var server = (0, _express.default)(); // Express Middlewares
// for logging express req and res

server.use((0, _morgan.default)('combined', {
  skip: function skip(req, res) {
    return res.statusCode < 400;
  }
}));
server.use((0, _cookieParser.default)());
server.use(_bodyParser.default.json());
server.use(_expressUseragent.default.express()); // configure remote logging
// if (!dev) {
//   Raven.config('https://30b971029d594608bb765ea6e46298f0@sentry.io/1207214', {
//     maxBreadcrumbs: 10,
//     sendTimeout: 5,
//   }).install();
//   server.use(compression());
// }

var RedisSessionStore = (0, _connectRedis.default)(_expressSession.default); // initialize redis store to be used by Ratelimiter

server.use((0, _expressSession.default)({
  key: 'ABCBDSESSID',
  store: new RedisSessionStore({
    prefix: 'starlight_session_',
    client: _redisConfig.default
  }),
  expireAfterSeconds: 3 * 60 * 60,
  // session is valid for 3 hours
  secret: _serverConfig.default.secret,
  httpOnly: true,
  resave: true,
  saveUninitialized: true,
  cookie: {
    secure: false
  }
}));
var app = (0, _next2.default)({
  dev: dev
});
var handle = app.getRequestHandler();
server.use('/*', _RateLimiter.default); // Security.js for protecting agains xss attacks

server.use(function (req, res, cb) {
  res.set('X-Powered-By', 'American Science CBD');
  res.set('X-XSS-Protection', 1);
  res.set('X-Frame-Options', 'SAMEORIGIN');
  res.set('Referrer-Policy', 'strict-origin');

  try {
    if (req.session) {
      // set key only for page requests
      // ignore for static calls and HMR calls in dev
      if (req.url.indexOf('/static/') === -1 && req.url.indexOf('on-demand-entries-ping') === -1) {
        res.set('ABCBDSESSID', req.sessionID);
      }

      if (req.session && !req.session.ip) {
        req.session.ip = _Security.default.getIp(req); // eslint-disable-line no-param-reassign
      }

      if (req.session && !req.session.userAgent) {
        req.session.userAgent = req.get('User-Agent'); // eslint-disable-line no-param-reassign
      }
    }
  } catch (error) {
    _raven.default.captureException(error);

    console.error('Exception Occurred in ReactApp', error.stack || error);
  }

  return cb();
});
app.prepare().then(function () {
  server.get('/promo/:useragent/*',
  /*#__PURE__*/
  function () {
    var _ref = _asyncToGenerator(
    /*#__PURE__*/
    _regenerator.default.mark(function _callee(req, res) {
      var requestAgent;
      return _regenerator.default.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              try {
                requestAgent = req.useragent.isMobile ? 'mobile' : 'desktop';
                res.redirect("/promo/".concat(requestAgent));
              } catch (error) {
                _raven.default.captureException(error);

                console.error('Exception Occurred in ReactApp', error.stack || error);
              }

            case 1:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, this);
    }));

    return function (_x, _x2) {
      return _ref.apply(this, arguments);
    };
  }());
  server.get('*', function (req, res) {
    try {
      var permittedRoutes = ['/', '/faqs', '/contact', '/products'];

      if (req.url.indexOf('/static/') === -1) {
        res.redirect('/index');
      }

      return handle(req, res);
    } catch (error) {
      _raven.default.captureException(error);

      console.error('Exception Occurred in ReactApp', error.stack || error);
    }
  });
  server.listen(port, function (err) {
    if (err) throw err;
    console.log("> Ready on ".concat(port));
  });
});