import { createStore, applyMiddleware } from "redux";
import rootReducer from "redux/reducers";

const bindMiddleware = middleware => {
  if (process.env.NODE_ENV !== "production") {
    // eslint-disable-next-line
    const { composeWithDevTools } = require("redux-devtools-extension");
    return composeWithDevTools(applyMiddleware());
  }
  return applyMiddleware();
};

export function configureStore(initialState = { auth: [] }) {
  const store = createStore(rootReducer, initialState, bindMiddleware());
  return store;
}
