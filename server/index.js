import express from "express";
import idx from "idx";
import next from "next";
import cookieParser from "cookie-parser";
import compression from "compression";
import useragent from "express-useragent";
import expressSession from "express-session";
import connectRedis from "connect-redis";
import querystring from "querystring";
import Raven from "raven";
import morgan from "morgan";
import axios from "axios";
import bodyParser from "body-parser";
import security from "./middlewares/Security";
import rateLimiter from "./middlewares/RateLimiter";
import config from "./server-config";
import redis from "./redis-config";

require("dotenv").config();

const {
  PORT,
  NODE_ENV,
  API_BASE_URL,
  ABTASTY_BASE_URL,
  ABTASTY_API_KEY
} = process.env;

const dev = NODE_ENV !== "production";

const port = PORT ? parseInt(PORT, 10) : 3000;

const server = express();

// Express Middlewares

// for logging express req and res
server.use(
  morgan("combined", {
    skip(req, res) {
      return res.statusCode < 400;
    }
  })
);

server.use(cookieParser());
server.use(bodyParser.json());
server.use(useragent.express());

// configure remote logging
// if (!dev) {
//   Raven.config('https://30b971029d594608bb765ea6e46298f0@sentry.io/1207214', {
//     maxBreadcrumbs: 10,
//     sendTimeout: 5,
//   }).install();
//   server.use(compression());
// }

const RedisSessionStore = connectRedis(expressSession);

// initialize redis store to be used by Ratelimiter
server.use(
  expressSession({
    key: "ABCBDSESSID",
    store: new RedisSessionStore({
      prefix: "starlight_session_",
      client: redis
    }),
    expireAfterSeconds: 3 * 60 * 60, // session is valid for 3 hours
    secret: config.secret,
    httpOnly: true,
    resave: true,
    saveUninitialized: true,
    cookie: {
      secure: false
    }
  })
);

const app = next({ dev });
const handle = app.getRequestHandler();
server.use("/*", rateLimiter);

// Security.js for protecting agains xss attacks
server.use((req, res, cb) => {
  res.set("X-Powered-By", "American Science CBD");
  res.set("X-XSS-Protection", 1);
  res.set("X-Frame-Options", "SAMEORIGIN");
  res.set("Referrer-Policy", "strict-origin");
  try {
    if (req.session) {
      // set key only for page requests
      // ignore for static calls and HMR calls in dev
      if (
        req.url.indexOf("/static/") === -1 &&
        req.url.indexOf("on-demand-entries-ping") === -1
      ) {
        res.set("ABCBDSESSID", req.sessionID);
      }

      if (req.session && !req.session.ip) {
        req.session.ip = security.getIp(req); // eslint-disable-line no-param-reassign
      }

      if (req.session && !req.session.userAgent) {
        req.session.userAgent = req.get("User-Agent"); // eslint-disable-line no-param-reassign
      }
    }
  } catch (error) {
    Raven.captureException(error);
    console.error("Exception Occurred in ReactApp", error.stack || error);
  }
  return cb();
});

app.prepare().then(() => {
  server.get("/about", (req, res) => {
    return app.render(req, res, "/about", {
      product: "warming_balm"
    });
  });

  server.get("*", (req, res) => {
    return app.render(req, res, "/index", {
      product: "warming_balm"
    });
  });

  server.listen(port, err => {
    if (err) throw err;
    console.log(`> Ready on ${port}`);
  });
});
